# Fatih Erikli
def here(path):
	import os
	dir = os.path.dirname(__file__)
	return os.path.join(dir, path)

# Mert Diricanli
def breadcrumbs(urlpath):
	urlPart = ''
	liste = []
	for part in urlpath.split('/'):
		if not part == '':
			urlPart = urlPart + part + '/'
			liste.append(urlPart)
	return liste