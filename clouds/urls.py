from django.conf.urls import patterns, include, url
import settings
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
	(r'^', include('web.urls')),
	(r'^facebook/', include('facebookAuth.urls')),
	(r'^google/', include('googleAuth.urls')),
	(r'^dropbox/', include('dropboxApp.urls')),
	# Examples:
	# url(r'^$', 'clouds.views.home', name='home'),
	# url(r'^clouds/', include('clouds.foo.urls')),

	# Uncomment the admin/doc line below to enable admin documentation:
	# url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

	# Uncomment the next line to enable the admin:
	url(r'^admin/', include(admin.site.urls)),
	url(r'^assets/(?P<path>.*)$', 'django.views.static.serve', {'document_root':settings.MEDIA_ROOT}),
)
