from django.conf.urls import patterns, include, url
import clouds.settings

urlpatterns = patterns('dropboxApp.views',
	url(r'^authapp/$','auth_dropbox_start', name="authDropboxUrl"),
	url(r'^authapp_finish/$','auth_dropbox_finish', name="authDropboxFinishUrl"),
	#url(r'^deneme/$','deneme', name="deneme"),
)