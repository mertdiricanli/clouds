from django.db import models
from django.contrib.auth.models import User

class DropboxUser(models.Model):
	user = models.OneToOneField(User)
	access_token = models.TextField()
	dropbox_id = models.CharField(max_length=100, unique=True, db_index=True)

	def __unicode__(self):
		return unicode(self.user)

