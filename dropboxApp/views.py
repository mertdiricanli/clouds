# Create your views here.
import dropbox
from dropbox.client import DropboxOAuth2Flow, DropboxClient
from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect, Http404
import urllib
from dropboxApp.models import DropboxUser
from django.contrib.auth.models import User

def get_dropbox_auth_flow(web_app_session):
	app_key = 'xj6kabc6oe17b56'
	app_secret = 'jxukcxa8kuljzed'
	redirect_url = 'http://127.0.0.1:8000/dropbox/authapp_finish/'
	return DropboxOAuth2Flow(app_key, app_secret, redirect_url, web_app_session, "dropbox-auth-csrf-token")

def auth_dropbox_start(request):
	authorize_url = get_dropbox_auth_flow(request.session).start()
	return HttpResponseRedirect(authorize_url)

def auth_dropbox_finish(request):
	requested_user = request.user
	try:
		access_token, user_id, url_state = \
		get_dropbox_auth_flow(request.session).finish(request.GET)
		create_user(requested_user,user_id,access_token)
	except DropboxOAuth2Flow.BadRequestException, e:
		# @todo OAuth hatalarini yakalayacagiz. 1/2
		return HttpResponseRedirect('/')
	except DropboxOAuth2Flow.BadStateException, e:
		# Start the auth flow again.
		authorize_url = get_dropbox_auth_flow(request.session).start()
		HttpResponseRedirect(authorize_url)
	except DropboxOAuth2Flow.CsrfException, e:
		http_status(403)
	except DropboxOAuth2Flow.NotApprovedException, e:
		return HttpResponseRedirect('/')
	except DropboxOAuth2Flow.ProviderException, e:
		# @todo OAuth hatalarini yakalayacagiz. 2/2
		return HttpResponseRedirect('/')

	return HttpResponseRedirect('/')

def create_user(requested_user, profile, access_token):
	""" Adds user's access_token to the database """
	user_is_created = False
	try:
		dropbox_user = DropboxUser.objects.get(dropbox_id=profile)
	except DropboxUser.DoesNotExist:
		dropbox_user = DropboxUser()
		dropbox_user.user_id = requested_user.id
		dropbox_user.access_token = access_token
		dropbox_user.dropbox_id = profile
		dropbox_user.save()


