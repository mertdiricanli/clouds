from django.conf.urls import patterns, include, url
import clouds.settings

urlpatterns = patterns('googleAuth.views',
	url(r'^login/$','login_handler', name="googleLoginUrl"),
)