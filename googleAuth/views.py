# -*- coding:utf-8 -*-
import datetime
import json
import urllib
import urlparse
import clouds.settings as settings
from googleAuth import utils
from googleAuth.models import *
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render, redirect, render_to_response
from django.template import RequestContext
from django.contrib import messages
from django.contrib.auth import login, authenticate, logout
from django.contrib.auth.models import User

def login_handler(request):
	if request.user.is_authenticated():
		return HttpResponseRedirect('/')

	if 'error' in request.GET:
		messages.add_message(request, messages.ERROR,
			request.GET['error'])
		return HttpResponseRedirect('/')
	elif 'code' in request.GET:
		params = { \
			'client_id': settings.GOOGLE_CLIENT_ID, \
			'redirect_uri': settings.GOOGLE_REDIRECT_URI, \
			'client_secret': settings.GOOGLE_CLIENT_SECRET, \
			'code': request.GET['code'], \
			'grant_type': 'authorization_code', \
		}
		req = urllib.urlopen('https://accounts.google.com/o/oauth2/token',
			urllib.urlencode(params))
		if req.getcode() != 200:
			response = render_to_response('web/500.html', {}, \
					context_instance=RequestContext(request))
			response.status_code = 500
			return response

		response = req.read()
		response_query_dict = json.loads(response)
		access_token = response_query_dict['access_token']
		expires_in = response_query_dict['expires_in']
		profile = utils.api('people/me', {'access_token': access_token})
		getemailaddress = utils.get_email_address({'access_token': access_token})
		emailaddress = getemailaddress['email']
		googleplus_user = create_or_update(profile, emailaddress, access_token, expires_in)

		user = authenticate(googleplus_user=googleplus_user)
		if user is not None:
			if user.is_active:
				login(request, user)
				request.session.set_expiry(googleplus_user.expiry_at)
				if 'next' in request.GET:
					return HttpResponseRedirect(request.GET['next'])
				return HttpResponseRedirect('/')
			else:
				messages.add_message(request, messages.ERROR, "Account disabled.")
		else:
			messages.add_message(request, messages.ERROR, "Login failed.")
	else:
		params = { 
			'client_id': settings.GOOGLE_CLIENT_ID, 
			'redirect_uri': settings.GOOGLE_REDIRECT_URI, 
			'scope': 'https://www.googleapis.com/auth/plus.me', 
			'scope': 'https://www.googleapis.com/auth/userinfo.email',
			'response_type': 'code', 
		}
		return HttpResponseRedirect('https://accounts.google.com/o/oauth2/auth?' +
			urllib.urlencode(params)
		)
def create_or_update(profile, emailaddress, access_token, expires_in):
	"""Creates or updates a Google+ user profile in local database.
	"""
	user_is_created = False
	try:
		googleplus_user = googleUser.objects.get(plus_id=profile['id'])
	except googleUser.DoesNotExist:
		first_name, last_name = get_first_and_last_name(profile['displayName'])
		user = User.objects.create( \
			first_name=first_name,
			last_name=last_name,
			email=emailaddress,
			username='gp_' + profile['id']
		)
		user_is_created = True

	if user_is_created:
		googleplus_user = googleUser()
		googleplus_user.plus_id = profile['id']
		googleplus_user.user = user
	else:
		first_name, last_name = get_first_and_last_name(profile['displayName'])
		googleplus_user.user.first_name = first_name
		googleplus_user.last_name = last_name

	googleplus_user.plus_display_name = profile['displayName']
	googleplus_user.access_token = access_token

	googleplus_user.save()

	return googleplus_user

def get_first_and_last_name(display_name):
	try:
		first_name, last_name = display_name.strip().rsplit(' ', 1)
	except ValueError:
		first_name = display_name
		last_name = ''
	return first_name, last_name


