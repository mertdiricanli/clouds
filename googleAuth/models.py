from django.db import models
from django.contrib.auth.models import User

class googleUser(models.Model):
	user = models.OneToOneField(User)
	access_token = models.TextField()
	expiry_at = models.DateTimeField(null=True)
	plus_id = models.CharField(max_length=100, unique=True, db_index=True)
	plus_display_name = models.CharField(max_length=100)
	