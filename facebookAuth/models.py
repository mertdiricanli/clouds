from django.db import models
from django.contrib.auth.models import User

class FacebookUser(models.Model):
	user = models.OneToOneField(User)
	access_token = models.TextField()
	expiry_at = models.DateTimeField(null=True)
	fb_id = models.CharField(max_length=100, unique=True, db_index=True)
	fb_username = models.CharField(max_length=100, unique=True, db_index=True)
	
	def __unicode__(self):
		return unicode(self.id) + u' | ' + unicode(self.fb_id)

