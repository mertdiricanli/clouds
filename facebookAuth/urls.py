from django.conf.urls import patterns, include, url
import clouds.settings

urlpatterns = patterns('facebookAuth.views',
	url(r'^login/$','login_handler', name="facebookLoginUrl"),
)