# Create your views here.
from django.http import HttpResponse, HttpResponseRedirect
from facebookAuth import utils
from facebookAuth.models import FacebookUser
from django.template.context import RequestContext
from django.shortcuts import render_to_response
from django.contrib import messages
from django.contrib.auth import authenticate, login
from django.contrib.auth.models import User
import clouds.settings as settings
import urllib
import urlparse
import datetime
import json

def login_handler(request):
	""" Facebook login handler """
	if request.user.is_authenticated():
		return HttpResponseRedirect(settings.LOGIN_REDIRECT_URL)

	if 'error' in request.GET:
		""" Error message reqired """
		return HttpResponseRedirect(settings.LOGIN_REDIRECT_URL)

	elif 'code' in request.GET:
		params = {
			'client_id' : settings.FACEBOOK_APP_ID,
			'redirect_uri' : settings.REDIRECT_URI,
			'client_secret' : settings.FACEBOOK_APP_SECRET,
			'code' : request.GET['code'], 
		}
		req = urllib.urlopen('https://graph.facebook.com/oauth/access_token?' +
			urllib.urlencode(params))

		if req.getcode() != 200:
			response = render_to_response('web/500.html', {},
				context_instance=RequestContext(request))
			response.status_code = 500
			return response

		response = req.read()
		response_query_dict = dict(urlparse.parse_qsl(response))
		access_token = response_query_dict['access_token']
		expires = response_query_dict['expires']
		profile = utils.graph_api('/me', {'access_token': access_token})

		fb_user = create_or_update(profile, access_token, expires)

		user = authenticate(fb_user=fb_user)
		if user is not None:
			if user.is_active:
				login(request, user)
				request.session
				if 'next' in request.GET:
					return HttpResponseRedirect(request.GET['next'])
				return HttpResponseRedirect(settings.LOGIN_REDIRECT_URL)
			else:
				messages.add_message(request, messages.ERROR, "Account disabled.")
		else:
			messages.add_message(request, messages.ERROR, "Login failed.")

	else:
		params = {
			'client_id'		: settings.FACEBOOK_APP_ID,
			'redirect_uri'	: settings.REDIRECT_URI,
			'scope'			: 'email',
		}
		return HttpResponseRedirect('https://www.facebook.com/dialog/oauth?'+
			urllib.urlencode(params))


def create_or_update(profile, access_token, expires):
	user_is_created = False

	try:
		fb_user = FacebookUser.objects.get(fb_id=profile['id'])
	except FacebookUser.DoesNotExist:
		user = User.objects.create(
			first_name=profile['first_name'],
			last_name=profile['last_name'],
			username='fb_' + profile['id'])
		user_is_created = True

	if user_is_created:
		fb_user = FacebookUser()
		fb_user.fb_id = profile['id']
		fb_user.user = user
	else:
		fb_user.user.first_name = profile['first_name']
		fb_user.last_name = profile['last_name']

	fb_user.fb_username = profile['username']
	fb_user.access_token = access_token

	fb_user.save()

	return fb_user
