from web.forms import AuthForm, CreateUserForm
def printAuthForm(request):
	return{
		'auth_form': AuthForm()
	}

def printCreateUserForm(request):
	return{
		'user_form': CreateUserForm()
	}