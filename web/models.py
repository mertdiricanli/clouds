from django.db import models
from django.contrib.auth.models import User

class UserProfile(models.Model):
	user = models.OneToOneField(User)
	def upload_path(self, filename):
		return 'uploads/%s_%s' % (self.date.strftime("%Y%m%d_%H%M%S"), filename)
	avatar = models.ImageField(upload_to=upload_path)
