from django import template
register = template.Library()
@register.filter
def breadcrumb(value, string="/"):
	parts = value.split(string)
	result = None
	for part in parts:
		if part == '' or part == None:
			pass
		else:
			result = part
	return result

