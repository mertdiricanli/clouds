from django.conf.urls import patterns, include, url
import clouds.settings

urlpatterns = patterns('web.views',
	url(r'^$','home', name="homeUrl"),
	url(r'^files(?P<getFile>.*)$','files', name="filesUrl"),
	url(r'^test(?P<filepath>.*)$','delete_files', name="delete_filesUrl"),
	url(r'^signup/$', 'signup', name="signupUrl"),
	url(r'^login/$', 'login_view', name="loginUrl"),
	url(r'^logout/$', 'logout_view', name="logoutUrl"),
)