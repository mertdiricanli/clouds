# -*- coding: utf-8 -*-
from web.models import *
from django.shortcuts import render, redirect
from django.http import HttpResponse, Http404, HttpResponseRedirect
from web.forms import CreateUserForm, AuthForm
from django.contrib.auth import login, authenticate, logout
from dropboxApp.models import DropboxUser
from dropbox.client import DropboxClient
from dropbox.rest import ErrorResponse
from clouds.utils import breadcrumbs

def home(request):
	if request.user.is_authenticated():
		current_user_id = request.user.id
		try:
			dropbox_user = DropboxUser.objects.get(user=current_user_id)
			return render(request, 'web/index.html', {'dropbox_user': dropbox_user})
		except DropboxUser.DoesNotExist:
			return render(request, 'web/index.html')
	else:
		return render(request, 'web/index.html')

def files(request, getFile):
	if request.user.is_authenticated():
		current_user_id = request.user.id
		try:
			dropbox_user = DropboxUser.objects.get(user=current_user_id)
			client = DropboxClient(dropbox_user.access_token)
			breadcrumb = breadcrumbs(request.path)
			try:
				dropbox_metadata = client.metadata(getFile)
			except ErrorResponse,e:
				raise Http404()
			return render(request, 'web/files.html', {'dropbox_user': dropbox_user,
				'dropbox_metadata': dropbox_metadata, 'breadcrumb': breadcrumb})
		except DropboxUser.DoesNotExist:
			return render(request, 'web/files.html')
	else:
		return render(request, 'web/files.html')

def delete_files(request, filepath):
	if request.user.is_authenticated():
		if request.is_ajax():
			current_user_id = request.user.id
			dropbox_user = DropboxUser.objects.get(user=current_user_id)
			client = DropboxClient(dropbox_user.access_token)
			try:
				result = client.file_delete(filepath)
			except ErrorResponse, e:
				raise Http404()
			return HttpResponse(result)
	else:
		return HttpResponseRedirect('/')

def signup(request):
	user_form = CreateUserForm(data=request.POST)
	if request.method == 'POST':
		if user_form.is_valid():
			username = user_form.clean_username()
			password = user_form.clean_password2()
			user_form.save()
			user = authenticate(username=username, password=password)
			login(request,user)
			return redirect('/')
		else:
			return home(request, user_form=user_form)
	return redirect('/')

def login_view(request):
	if request.method == 'POST':
		form = AuthForm(data=request.POST)
		if form.is_valid():
			login(request,form.get_user())
			return redirect('/')
		else:
			return home(request, auth_form=form)
	return redirect('/')

def logout_view(request):
	logout(request)
	return redirect('/')
