# -*- coding:utf-8 -*-
from django.contrib.auth.forms import AuthenticationForm, UserCreationForm
from django.contrib.auth.models import User
from django import forms
from django.utils.html import strip_tags

class CreateUserForm(UserCreationForm):
	email = forms.EmailField(required=True, widget=forms.widgets.TextInput(attrs={'placeholder':'Email','class':'form-control'}))
	username = forms.CharField(required=True, widget=forms.widgets.TextInput(attrs={'placeholder':'Username','class':'form-control'}))
	first_name = forms.CharField(required=True, widget=forms.widgets.TextInput(attrs={'placeholder':'Name','class':'form-control'}))
	last_name = forms.CharField(required=True, widget=forms.widgets.TextInput(attrs={'placeholder':'Surname','class':'form-control'}))
	password1 = forms.CharField(required=True, widget=forms.widgets.PasswordInput(attrs={'placeholder':'Password','class':'form-control'}))
	password2 = forms.CharField(required=True, widget=forms.widgets.PasswordInput(attrs={'placeholder':'Password Again','class':'form-control'}))

	def is_valid(self):
		form = super(CreateUserForm,self).is_valid()
		for f, error in self.errors.iteritems():
			if f != '__all__':
				self.fields[f].widget.attrs.update({'class':'error', 'value': strip_tags(error)})
		return form

	class Meta:
		fields = ['first_name','last_name','username','email','password1','password2']
		model = User

class AuthForm(AuthenticationForm):
	username = forms.CharField(widget=forms.widgets.TextInput(attrs={'placeholder':'Username','class':'form-control'}))
	password = forms.CharField(widget=forms.widgets.PasswordInput(attrs={'placeholder':'Password','class':'form-control'}))

	def is_valid(self):
		form = super(AuthForm,self).is_valid()
		for f, error in self.errors.iteritems():
			if f != '__all__':
				self.fields[f].widget.attrs.update({'placeholder': strip_tags(error)})
		return form


